#include "./ini_utils.h"
#include <string.h>
#include <stdlib.h>
#include "./ini.h"

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, #n) == 0

#define POPULATE_CONFIG_KEY_VAL(section, key) \
	if (MATCH(section, key))              \
	{                                     \
		pconfig->key = strdup(value); \
		pconfig->defined_##key = 1; \
		return 1;                     \
	}

#define POPULATE_INT_CONFIG_KEY_VAL(section, key) \
	if (MATCH(section, key))              \
	{                                     \
		pconfig->key = atoi(strdup(value)); \
		return 1;                     \
	}

#define POPULATE_CONFIG_KEY_VAL_RANGE(key, n) \
	POPULATE_CONFIG_KEY_VAL("SpecialSet", key##n)

#define INIT_NULL(config, key, n) config->key##n = NULL;

static int read_ini_handler(void *config, const char *section, const char *name,
			    const char *value)
{
	configuration *pconfig = (configuration *)config;
	const char *section_name = "Basic_Threshold";

	POPULATE_INT_CONFIG_KEY_VAL(section_name, SCapCbTest_OFF_Min)
	POPULATE_INT_CONFIG_KEY_VAL(section_name, SCapCbTest_OFF_Max)
	POPULATE_INT_CONFIG_KEY_VAL(section_name, SCapCbTest_ON_Min)
	POPULATE_INT_CONFIG_KEY_VAL(section_name, SCapCbTest_ON_Max)

	POPULATE_INT_CONFIG_KEY_VAL(section_name, SCapRawDataTest_OFF_Min)
	POPULATE_INT_CONFIG_KEY_VAL(section_name, SCapRawDataTest_OFF_Max)
	POPULATE_INT_CONFIG_KEY_VAL(section_name, SCapRawDataTest_ON_Min)
	POPULATE_INT_CONFIG_KEY_VAL(section_name, SCapRawDataTest_ON_Max)

	POPULATE_INT_CONFIG_KEY_VAL(section_name, FW_VER_VALUE)

	POPULATE_INT_CONFIG_KEY_VAL(section_name, PanelDifferTest_Min);
	POPULATE_INT_CONFIG_KEY_VAL(section_name, PanelDifferTest_Max);

	POPULATE_INT_CONFIG_KEY_VAL(section_name, RawDataTest_Low_Min);
	POPULATE_INT_CONFIG_KEY_VAL(section_name, RawDataTest_Low_Max);
	POPULATE_INT_CONFIG_KEY_VAL(section_name, RawDataTest_High_Min);
	POPULATE_INT_CONFIG_KEY_VAL(section_name, RawDataTest_High_Max);

	ONE_THROUGH_16(POPULATE_CONFIG_KEY_VAL_RANGE, RawData_Max_High_Tx)
	ONE_THROUGH_16(POPULATE_CONFIG_KEY_VAL_RANGE, RawData_Min_High_Tx)
	ONE_THROUGH_16(POPULATE_CONFIG_KEY_VAL_RANGE, Panel_Differ_Max_Tx)
	ONE_THROUGH_16(POPULATE_CONFIG_KEY_VAL_RANGE, Panel_Differ_Min_Tx)

	return 0;
}

struct array_of_values parse_array_of_values(const char *array_string)
{
	int no_of_commas = 0;
	int slen = strlen(array_string);
	if (slen == 0) {
		struct array_of_values ret;
		ret.length = 0;
		ret.array = NULL;
		return ret;
	}

	for (int i = 0; i < slen; i++) {
		if (array_string[i] == ',') {
			no_of_commas += 1;
		}
	}

	int *array = (int *)malloc(sizeof(int) * no_of_commas);

	int start = 0;
	for (int i = 0; i < no_of_commas; i++) {
		int j = 0;
		char numval[50];

		int next_char_idx = start;
		int real_char_offset = 0;

		while (array_string[next_char_idx] != ',') {
			if (array_string[next_char_idx] <= '9' && array_string[next_char_idx] >= '0') {
				numval[real_char_offset] = array_string[next_char_idx];
				real_char_offset++;
			}
			j++;
			next_char_idx = start + j;
		}

		numval[real_char_offset] = '\0';
		start = next_char_idx + 1;

		array[i] = atoi(numval);
	}

	struct array_of_values ret;
	ret.length = no_of_commas;
	ret.array = array;
	return ret;
}

#define COUNT_TX(key, counter, n) \
	if (config.key##n != NULL) { \
		ret = parse_array_of_values(config.key##n); \
		counter += ret.length; \
		free(ret.array); \
	}

#define APPEND_TX_VAL(key, arr, n) \
	if (config.key##n != NULL) { \
		ret = parse_array_of_values(config.key##n); \
		for (int i = 0; i < ret.length; i++) { \
			arr.array[arr.length] = ret.array[i]; \
			arr.length++; \
		} \
		free(ret.array); \
	}

int read_config(configuration *pconfig)
{
	ONE_THROUGH_16(INIT_NULL, pconfig, RawData_Max_High_Tx)
	ONE_THROUGH_16(INIT_NULL, pconfig, RawData_Min_High_Tx)
	ONE_THROUGH_16(INIT_NULL, pconfig, Panel_Differ_Max_Tx)
	ONE_THROUGH_16(INIT_NULL, pconfig, Panel_Differ_Min_Tx)

	if (ini_parse("test.ini", read_ini_handler, pconfig) < 0) {
		printf("Can't load 'test.ini'\n");
		return 1;
	}
	configuration config = *pconfig;

	int total_rawdata_max_high_tx = 0;
	int total_rawdata_min_high_tx = 0;
	int total_panel_differ_max_tx = 0;
	int total_panel_differ_min_tx = 0;

	struct array_of_values ret;
	ONE_THROUGH_16(COUNT_TX, RawData_Max_High_Tx, total_rawdata_max_high_tx)
	ONE_THROUGH_16(COUNT_TX, RawData_Min_High_Tx, total_rawdata_min_high_tx)
	ONE_THROUGH_16(COUNT_TX, Panel_Differ_Max_Tx, total_panel_differ_max_tx)
	ONE_THROUGH_16(COUNT_TX, Panel_Differ_Min_Tx, total_panel_differ_min_tx)

	int* rawdata_max_high_tx_all = (int*)malloc(sizeof(int) * total_rawdata_max_high_tx);
	int* rawdata_min_high_tx_all = (int*)malloc(sizeof(int) * total_rawdata_min_high_tx);
	int* panel_differ_max_tx_all = (int*)malloc(sizeof(int) * total_panel_differ_max_tx);
	int* panel_differ_min_tx_all = (int*)malloc(sizeof(int) * total_panel_differ_min_tx);

	struct array_of_values rawdata_max_high_tx;
	rawdata_max_high_tx.array = rawdata_max_high_tx_all;
	rawdata_max_high_tx.length = 0;
	ONE_THROUGH_16(APPEND_TX_VAL, RawData_Max_High_Tx, rawdata_max_high_tx)
	pconfig->RawData_Max_High_Tx.array = rawdata_max_high_tx.array;
	pconfig->RawData_Max_High_Tx.length = rawdata_max_high_tx.length;

	struct array_of_values rawdata_min_high_tx;
	rawdata_min_high_tx.array = rawdata_min_high_tx_all;
	rawdata_min_high_tx.length = 0;
	ONE_THROUGH_16(APPEND_TX_VAL, RawData_Min_High_Tx, rawdata_min_high_tx)
	pconfig->RawData_Min_High_Tx.array = rawdata_min_high_tx.array;
	pconfig->RawData_Min_High_Tx.length = rawdata_min_high_tx.length;

	struct array_of_values panel_differ_max_tx;
	panel_differ_max_tx.array = panel_differ_max_tx_all;
	panel_differ_max_tx.length = 0;
	ONE_THROUGH_16(APPEND_TX_VAL, Panel_Differ_Max_Tx, panel_differ_max_tx)
	pconfig->Panel_Differ_Max_Tx.array = panel_differ_max_tx.array;
	pconfig->Panel_Differ_Max_Tx.length = panel_differ_max_tx.length;

	struct array_of_values panel_differ_min_tx;
	panel_differ_min_tx.array = panel_differ_min_tx_all;
	panel_differ_min_tx.length = 0;
	ONE_THROUGH_16(APPEND_TX_VAL, Panel_Differ_Min_Tx, panel_differ_min_tx)
	pconfig->Panel_Differ_Min_Tx.array = panel_differ_min_tx.array;
	pconfig->Panel_Differ_Min_Tx.length = panel_differ_min_tx.length;

	set_is_defined(pconfig);
	return 0;
}

void free_array_of_values(struct array_of_values x) {
	free(x.array);
}

#define check_is_defined_and_set(key, n) \
	pconfig->is_defined_##key &= pconfig->defined_##key##n; \

#define is_defined_16(key) \
	pconfig->is_defined_##key = 1; \
	ONE_THROUGH_16(check_is_defined_and_set, key)

void set_is_defined(configuration* pconfig) {
	is_defined_16(RawData_Max_High_Tx)
	is_defined_16(RawData_Min_High_Tx)
	is_defined_16(Panel_Differ_Max_Tx)
	is_defined_16(Panel_Differ_Min_Tx)
}
