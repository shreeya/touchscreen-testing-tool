# touchscreen-testing-tool

## Compile
`gcc ini.c ini_utils.c testing_tool.c -o testing_tool.o -li2c`

## Run
```
Usage:		./testing_tool -d <device> -v/r/s/q/p/a
Example:	./testing_tool -d /dev/i2c-1 -a
Parameters:
	-d <device>	Device to be tested
	-v		Firmware Version Test
	-r		Rawdata Test
	-s		Scap CB Test
	-q		Scap Rawdata Test
	-p		Panel Differ Test
	-i		Display IC information
	-a		Run all tests
```

## To use different version of ini file, copy it to test.ini

test.ini file is parsed at runtime so copy any one of the required ini file to it.

`cp Valve_Quanta_FT3519T_V0x01_20230206.ini test.ini`

`cp 7Valve-FT3528U-TLCM-20220302.ini test.ini`

`cp 7Valve-KH-8.20_1.ini test.ini`
