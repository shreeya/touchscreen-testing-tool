#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <i2c/smbus.h>
#include "./ini_utils.h"

#define I2C_DEV_ADDR 0x38

#define DEBUG 0

#if DEBUG
#define DEBUG_PRINT(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__);
#else
#define DEBUG_PRINT(fmt, ...)
#endif

int fd;

void usage()
{
	fprintf(stderr, "Usage:\t\t./testing_tool -d <device> -v/r/s/q/p/a\n");
	fprintf(stderr, "Example:\t./testing_tool -d /dev/i2c-1 -a\n");
	fprintf(stderr, "Parameters:\n");
	fprintf(stderr, "\t-d <device>\tDevice to be tested\n");
	fprintf(stderr, "\t-v\t\tFirmware Version Test\n");
	fprintf(stderr, "\t-r\t\tRawdata Test\n");
	fprintf(stderr, "\t-s\t\tScap CB Test\n");
	fprintf(stderr, "\t-q\t\tScap Rawdata Test\n");
	fprintf(stderr, "\t-p\t\tPanel Differ Test\n");
	fprintf(stderr, "\t-i\t\tDisplay IC information\n");
	fprintf(stderr, "\t-a\t\tRun all tests\n");

	exit(EXIT_FAILURE);
}

static int switch_hid_to_i2c(void)
{
	unsigned char buf[3] = {0};

	buf[0] = 0xeb;
	buf[1] = 0xaa;
	buf[2] = 0x09;

	if (write(fd, buf, 3) != 3) {
		printf("\n Failed to write the buffer to switch to I2C mode");
		return -1;
	}

	buf[0] = buf[1] = buf[2] = 0;

	if (read(fd, buf, 3) != 3) {
		printf("\n Failed to read the buffer to confirm I2C mode");
		return -1;
	}

	if (buf[0] == 0xEB && buf[1] == 0xAA && buf[2] == 0x08) {
		return 0;
	}
	else
		return -1;
}

static int switch_i2c_to_hid(void)
{
	if (i2c_smbus_write_byte_data(fd, 0x00, 0x07) < 0) {
		printf("\n Failed to write 0x07 into 0x00 register");
		return -1;
	}

	if (i2c_smbus_write_byte_data(fd, 0xfc, 0xaa) < 0) {
		printf("\n Failed to write 0xaa into 0xfc register");
		return -1;
	}

	usleep(80000);

	if (i2c_smbus_write_byte_data(fd, 0xfc, 0x66) < 0) {
		printf("\n Failed to write 0x66 into 0xfc register");
		return -1;
	}

	return 0;
}

static int enter_upgrade_mode(void)
{
	if (i2c_smbus_write_byte_data(fd, 0x00, 0x40) < 0) {
		printf("Device failed to enter upgrade mode\n");
		return -1;
	}

	usleep(500000);

	return 0;
}
static int read_register(unsigned char read_buf[], int len)
{
	if (write(fd, read_buf, 1) != 1) {
		printf("\n Failed to write the value 0x%x", read_buf[0]);
		return -1;
	}

	if (read(fd, read_buf, len) != len) {
		printf("\n Failed to read buffer from the 0x%x register",
		       read_buf[0]);
		return -1;
	}
	return 0;
}

int test_data_waterproof_mode(unsigned char rx, unsigned char tx, unsigned char reg)
{
	int num, bit6, bit5, bit2;

	bit6 = (reg >> 6) & 1;
	bit5 = (reg >> 5) & 1;
	bit2 = (reg >> 2) & 1;

	if (bit5 == 0) {
		if (bit6 == 1) {
			if (bit2 == 0)
				num = tx;
			else
				num = rx;
		}
		else
			num = rx + tx;
	}
	else {
		printf("\n No Waterproof detection");
		return -1;
	}

	num = num * 2;
	return num;
}

int test_data_normal_mode(unsigned char rx, unsigned char tx, unsigned char reg)
{
	int num, bit7, bit1, bit0;

	bit7 = (reg >> 7) & 1;
	bit1 = (reg >> 1) & 1;
	bit0 = (reg >> 0) & 1;

	if (bit7 == 0) {
		if ((bit1 + bit0) == 0)
			num = tx;
		else if (bit1 + bit0 == 1)
			num = rx;
		else if (bit1 + bit0 == 2)
			num = rx + tx;
		else {
			printf("\n Error in fetching the normal mode test data count");
			return -1;
		}
	}
	else {
		printf("\n No Normal mode detection");
		return -1;
	}

	num = num * 2;
	return num;
}

static int confirm_scan_completion(void)
{
	unsigned char reg_val = 0;
	int retries = 5;

	if (i2c_smbus_write_byte_data(fd, 0x00, 0xc0) < 0) {
		printf("\n Failed to write 0xc0 into register 0x00");
		return -1;
	}

	while (reg_val != 64 && retries >= 0) {
		usleep(500000);
		reg_val = i2c_smbus_read_byte_data(fd, 0x00);
		retries--;
	}
	if (retries < 0)
		return -1;

	return 0;
}

static int version_check_test(configuration config)
{
	int ver = 0;

	ver = i2c_smbus_read_byte_data(fd, 0xA6);
	if (ver < 0) {
		printf("Invalid firmware version\n");
		return -1;
	}

	printf("Firmware version retrieved = %d\n", ver);
	if (ver != config.FW_VER_VALUE) {
		DEBUG_PRINT("Current firmware version is %d\n", ver);
		return -1;
	}

	return 0;
}

static int rawdata_test(configuration config)
{
	unsigned char rawdata_tx, rawdata_rx, rawdata_0a;
	int j = 0, actual, max, min;

	if (enter_upgrade_mode() < 0)
		return -1;

	rawdata_tx = i2c_smbus_read_byte_data(fd, 0x02);
	rawdata_rx = i2c_smbus_read_byte_data(fd, 0x03);
	rawdata_0a = i2c_smbus_read_byte_data(fd, 0x0a);

	if (rawdata_tx < 0 || rawdata_rx < 0 || rawdata_0a < 0) {
		printf("Unable to read rawdata registers\n");
		return -1;
	}

	if (i2c_smbus_write_byte_data(fd, 0x0a, 0x81) < 0) {
		printf("Failed to write 0x81 into register 0x0a\n");
		return -1;
	}

	for (int i = 0; i < 3; i++) {
		if (confirm_scan_completion() < 0) {
			printf("Error occurred in scanning\n");
			return -1;
		}
	}

	if (i2c_smbus_write_byte_data(fd, 0x01, 0xAA) < 0) {
		printf("Failed to write 0xAA into register 0x01\n");
		return -1;
	}

	int RAWDATA_SIZE = (rawdata_tx * rawdata_rx) * 2;

	unsigned char rawdata_buf[RAWDATA_SIZE];
	int buf[RAWDATA_SIZE / 2];
	rawdata_buf[0] = 0x36;

	if (read_register(rawdata_buf, RAWDATA_SIZE)) {
		printf("Failed to read rawdata buffer\n");
		return -1;
	}

	DEBUG_PRINT("Rawdata test data :-\n");
	for (int i = 0; i < RAWDATA_SIZE - 1; i += 2) {
		buf[j] = rawdata_buf[i] << 8 | rawdata_buf[i + 1];
		DEBUG_PRINT("%d\t", buf[j]);
		j++;
	}

	DEBUG_PRINT("\n");
	for (int i = 0; i < RAWDATA_SIZE / 2; i++) {
		actual = buf[i];
		max = config.is_defined_RawData_Max_High_Tx == 1 ? config.RawData_Max_High_Tx.array[i] : config.RawDataTest_High_Max;
		min = config.is_defined_RawData_Min_High_Tx == 1 ? config.RawData_Min_High_Tx.array[i] : config.RawDataTest_High_Min;
		if (actual < min || actual > max) {
			DEBUG_PRINT("%d value doesn't fall in between %d and %d\n",
				    actual, min, max);
			return -1;
		}
	}

	if (i2c_smbus_write_byte_data(fd, 0x0a, rawdata_0a) < 0) {
		printf("Failed to write 0x%x into register 0x0a\n", rawdata_0a);
		return -1;
	}

	return 0;
}

static int scap_cb_test(configuration config)
{
	unsigned char scap_cb_tx, scap_cb_rx, scap_cb_09;
	int j = 0, waterproof, normal;

	if (enter_upgrade_mode() < 0)
		return -1;

	/* Switch to no mapping status */

	if (i2c_smbus_write_byte_data(fd, 0x54, 0x01) < 0) {
		printf("Failed to write 0x01 into register 0x54\n");
		return -1;
	}

	scap_cb_tx = i2c_smbus_read_byte_data(fd, 0x55);
	scap_cb_rx = i2c_smbus_read_byte_data(fd, 0x56);
	scap_cb_09 = i2c_smbus_read_byte_data(fd, 0x09);

	if (scap_cb_tx < 0 || scap_cb_rx < 0 || scap_cb_09 < 0)
		printf("Unable to read scap cb data registers\n");

	/* Set waterproof mode */

	unsigned char reg_44 = i2c_smbus_read_byte_data(fd, 0x44);
	if (i2c_smbus_write_byte_data(fd, 0x44, 0x01) < 0) {
		printf("Failed to write 0x01 into register 0x44\n");
		return -1;
	}

	unsigned char reg_45 = i2c_smbus_read_byte_data(fd, 0x45);
	if (i2c_smbus_write_byte_data(fd, 0x45, 0x00) < 0) {
		printf("Failed to write 0x00 into register 0x45\n");
		return -1;
	}

	int SCAP_CB_DATA = test_data_waterproof_mode(scap_cb_tx, scap_cb_rx, scap_cb_09);
	unsigned char scap_cb_buf[SCAP_CB_DATA];
	int buf[SCAP_CB_DATA / 2];
	scap_cb_buf[0] = 0x4e;

	if (read_register(scap_cb_buf, SCAP_CB_DATA)) {
		printf("Failed to read scap cb waterproof mode buffer\n");
		return -1;
	}

	DEBUG_PRINT("Scap CB waterproof mode test data :-\n");
	for (int i = 0; i < SCAP_CB_DATA - 1; i += 2) {
		buf[j] = scap_cb_buf[i] << 8 | scap_cb_buf[i + 1];
		DEBUG_PRINT("%d\t", buf[j]);
		j++;
	}

	DEBUG_PRINT("\n");
	for( int i = 0; i < SCAP_CB_DATA / 2; i++) {
		waterproof = buf[i];
		if(waterproof < config.SCapCbTest_ON_Min ||
		   waterproof > config.SCapCbTest_ON_Max) {
			DEBUG_PRINT("%d value doesn't fall in between %d and %d\n",
				    waterproof, config.SCapCbTest_ON_Min, config.SCapCbTest_ON_Max);
			printf("Scap cb waterproof mode test failed\n");
			return -1;
		}
	}

	/* Set normal mode */

	if (i2c_smbus_write_byte_data(fd, 0x44, 0x00) < 0) {
		printf("Failed to write 0x00 into register 0x44\n");
		return -1;
	}

	if (i2c_smbus_write_byte_data(fd, 0x45, 0x00) < 0) {
		printf("Failed to write 0x00 into register 0x45\n");
		return -1;
	}

	SCAP_CB_DATA = test_data_normal_mode(scap_cb_tx, scap_cb_rx, scap_cb_09);
	unsigned char scap_cb_buf2[SCAP_CB_DATA];
	int buf2[SCAP_CB_DATA / 2];
	scap_cb_buf2[0] = 0x4e;
	j = 0;

	if (read_register(scap_cb_buf2, SCAP_CB_DATA)) {
		printf("Failed to read scap cb normal mode buffer\n");
		return -1;
	}

	DEBUG_PRINT("Scap CB normal mode test data :-\n");
	for (int i = 0; i < SCAP_CB_DATA - 1; i += 2) {
		buf2[j] = scap_cb_buf2[i] << 8 | scap_cb_buf2[i + 1];
		DEBUG_PRINT("%d\t", buf2[j]);
		j++;
	}

	DEBUG_PRINT("\n");
	for (int i = 0; i < SCAP_CB_DATA / 2; i++) {
		normal = buf2[i];
		if (normal < config.SCapCbTest_OFF_Min || normal > config.SCapCbTest_OFF_Max) {
			DEBUG_PRINT("%d value doesn't fall in between %d and %d\n",
				    normal, config.SCapCbTest_OFF_Min, config.SCapCbTest_OFF_Max);
			printf("Scap cb normal mode test failed\n");
			return -1;
		}
	}

	if (i2c_smbus_write_byte_data(fd, 0x44, reg_44) < 0) {
		printf("Failed to write 0x%x into register 0x44\n", reg_44);
		return -1;
	}

	if (i2c_smbus_write_byte_data(fd, 0x45, reg_45) < 0) {
		printf("Failed to write 0x%x into register 0x45\n", reg_45);
		return -1;
	}

	return 0;
}

static int scap_rawdata_test(configuration config)
{
	unsigned char scap_rw_tx, scap_rw_rx, scap_rw_09;
	int j = 0, waterproof, normal;

	if (enter_upgrade_mode() < 0)
		return -1;

	/* Switch to no mapping status */

	unsigned char reg_54 = i2c_smbus_read_byte_data(fd, 0x54);
	if (i2c_smbus_write_byte_data(fd, 0x54, 0x01) < 0) {
		printf("Failed to write 0x01 into register 0x54\n");
		return -1;
	}

	scap_rw_tx = i2c_smbus_read_byte_data(fd, 0x55);
	scap_rw_rx = i2c_smbus_read_byte_data(fd, 0x56);
	scap_rw_09 = i2c_smbus_read_byte_data(fd, 0x09);

	if (scap_rw_tx < 0 || scap_rw_rx < 0 || scap_rw_09 < 0)
		printf("Unable to read scap rawdata registers\n");

	if (confirm_scan_completion() < 0) {
		printf("Error occurred in scanning\n");
		return -1;
	}

	/* Set waterproof mode */

	if (i2c_smbus_write_byte_data(fd, 0x01, 0xAC) < 0) {
		printf("Failed to write 0xAC into register 0x01\n");
		return -1;
	}

	int SCAP_RW_DATA = test_data_waterproof_mode(scap_rw_tx, scap_rw_rx, scap_rw_09);

	unsigned char scap_rawdata_buf[SCAP_RW_DATA];
	int buf[SCAP_RW_DATA / 2];
	scap_rawdata_buf[0] = 0x36;

	if (read_register(scap_rawdata_buf, SCAP_RW_DATA)) {
		printf("Failed to read scap rawdata waterproof mode buffer\n");
		return -1;
	}

	DEBUG_PRINT("Scap rawdata waterproof mode test data :-\n");
	for (int i = 0; i < SCAP_RW_DATA - 1; i += 2) {
		buf[j] = scap_rawdata_buf[i] << 8 | scap_rawdata_buf[i + 1];
		DEBUG_PRINT("%d\t",buf[j]);
		j++;
	}

	DEBUG_PRINT("\n");
	for (int i = 0; i < SCAP_RW_DATA / 2; i++) {
		waterproof = buf[i];
		if (waterproof < config.SCapRawDataTest_ON_Min ||
		    waterproof > config.SCapRawDataTest_ON_Max) {
			DEBUG_PRINT("%d value doesn't fall in between %d and %d\n", waterproof,
				    config.SCapRawDataTest_ON_Min, config.SCapRawDataTest_ON_Max);
			printf("\n Scap rawdata waterproof mode test failed\n");
			return -1;
		}
	}

	/* Set normal mode */

	if (i2c_smbus_write_byte_data(fd, 0x01, 0xAB) < 0) {
		printf("Failed to write 0xAB into register 0x01\n");
		return -1;
	}

	SCAP_RW_DATA = test_data_normal_mode(scap_rw_tx, scap_rw_rx, scap_rw_09);
	unsigned char scap_rawdata_buf2[SCAP_RW_DATA];
	int buf2[SCAP_RW_DATA / 2];
	scap_rawdata_buf2[0] = 0x36;
	j = 0;

	if (read_register(scap_rawdata_buf2, SCAP_RW_DATA)) {
		printf("Failed to read scap rawdata normal mode buffer\n");
		return -1;
	}

	DEBUG_PRINT("Scap rawdata normal mode test data :-\n");
	for (int i = 0; i < SCAP_RW_DATA - 1; i += 2) {
		buf2[j] = scap_rawdata_buf2[i] << 8 | scap_rawdata_buf2[i + 1];
		DEBUG_PRINT("%d\t",buf2[j]);
		j++;
	}

	DEBUG_PRINT("\n");
	for (int i = 0; i < SCAP_RW_DATA / 2; i++) {
		normal = buf2[i];
		if (normal < config.SCapRawDataTest_OFF_Min ||
		    normal > config.SCapRawDataTest_OFF_Max) {
			DEBUG_PRINT("%d value doesn't fall in between %d and %d\n", normal,
				    config.SCapRawDataTest_OFF_Min, config.SCapRawDataTest_OFF_Max);
			printf("Scap rawdata normal mode test failed\n");
			return -1;
		}
	}

	if (i2c_smbus_write_byte_data(fd, 0x54, reg_54) < 0) {
		printf("Failed to write 0x%x into register 0x54\n", reg_54);
		return -1;
	}

	return 0;
}

static int panel_differ_test(configuration config)
{
	unsigned char pd_tx, pd_rx;
	int j = 0, min, max, actual;

	if (enter_upgrade_mode() < 0)
		return -1;

	pd_tx = i2c_smbus_read_byte_data(fd, 0x02);
	pd_rx = i2c_smbus_read_byte_data(fd, 0x03);

	if (pd_tx < 0 || pd_rx < 0) {
		printf("Unable to read panel differ registers\n");
		return -1;
	}

	unsigned char reg_0a = i2c_smbus_read_byte_data(fd, 0x0a);
	if (i2c_smbus_write_byte_data(fd, 0x0a, 0x81) < 0) {
		printf("Failed to write 0x81 into register 0x0a\n");
		return -1;
	}

	unsigned char reg_16 = i2c_smbus_read_byte_data(fd, 0x16);
	if (i2c_smbus_write_byte_data(fd, 0x16, 0x00) < 0) {
		printf("Failed to write 0x00 into register 0x16\n");
		return -1;
	}

	unsigned char reg_fb = i2c_smbus_read_byte_data(fd, 0xfb);
	if (i2c_smbus_write_byte_data(fd, 0xfb, 0x00) < 0) {
		printf("Failed to write 0x00 into register 0xfb\n");
		return -1;
	}

	for (int i = 0; i < 3; i++) {
		if (confirm_scan_completion() < 0) {
			printf("Error occurred in scanning\n");
			return -1;
		}
	}

	if (i2c_smbus_write_byte_data(fd, 0x01, 0xAA) < 0) {
		printf("Failed to write 0xAA into register 0x01\n");
		return -1;
	}

	int PD_DATA = (pd_tx * pd_rx) * 2;

	unsigned char pd_buf[PD_DATA];
	int buf[PD_DATA / 2];
	pd_buf[0] = 0x36;

	if (read_register(pd_buf, PD_DATA)) {
		printf("Failed to read panel differ buffer\n");
		return -1;
	}


	DEBUG_PRINT("Panel Differ Test Data :-\n");
	for (int i = 0; i < PD_DATA - 1; i += 2) {
		buf[j] = pd_buf[i] << 8 | pd_buf[i + 1];
		buf[j] = buf[j] / 10;
		DEBUG_PRINT("%d\t", buf[j]);
		j++;
	}

	DEBUG_PRINT("\n");
	for (int i = 0; i < PD_DATA / 2; i++) {
		min = config.is_defined_Panel_Differ_Min_Tx == 1 ? config.Panel_Differ_Min_Tx.array[i] : config.PanelDifferTest_Min;
		max = config.is_defined_Panel_Differ_Max_Tx == 1 ? config.Panel_Differ_Max_Tx.array[i] : config.PanelDifferTest_Max;
		actual = buf[i];
		if (actual < min || actual > max) {
			DEBUG_PRINT("%d value doesn't fall in between of %d and %d\n",
				    actual, min, max);
			return -1;
		}
	}

	if (i2c_smbus_write_byte_data(fd, 0x0a, reg_0a) < 0) {
		printf("Failed to write 0x%x into register 0x0a\n", reg_0a);
		return -1;
	}

	if (i2c_smbus_write_byte_data(fd, 0x16, reg_16) < 0) {
		printf("Failed to write 0x%x into register 0x16\n", reg_16);
		return -1;
	}

	if (i2c_smbus_write_byte_data(fd, 0xfb, reg_fb) < 0) {
		printf("Failed to write 0x%x into register 0xfb\n", reg_fb);
		return -1;
	}

	return 0;
}


int ic_display_info(configuration config) {

	unsigned char ic_buf[20];
	ic_buf[0] = 0x51;

	if (enter_upgrade_mode() < 0)
		return -1;

	if (i2c_smbus_write_byte_data(fd, 0x50, 0x20) < 0) {
		printf("Failed to write 0x20 into register 0x50\n");
		return -1;
	}

	if (read_register(ic_buf, 20)) {
		printf("Failed to read panel differ buffer\n");
		return -1;
	}

	DEBUG_PRINT("Project Code retrieved = %s\n", ic_buf);
	printf("Project Code: %s\n", ic_buf);
	return 0;
}

int main(int argc, char **argv)
{

	int ret = -1, opt, success = 0, version = 0;
	char *device;
	int rawdata = 0, scap_cb = 0, scap_rawdata = 0, panel_differ = 0, ic_info = 0;

	while ((opt = getopt(argc, argv, "d:vrsqpahil")) != -1) {
		switch (opt) {
		case 'd':
			device = strdup(optarg);
			if (!device)
				printf("Can't allocate memory.\n");
			break;

		case 'v':
			version = 1;
			break;
		case 'r':
			rawdata = 1;
			break;
		case 'q':
			scap_rawdata = 1;
			break;
		case 's':
			scap_cb = 1;
			break;
		case 'p':
			panel_differ = 1;
			break;
		case 'i':
			ic_info = 1;
			break;
		case 'a':
			version = rawdata = scap_cb = scap_rawdata = panel_differ = ic_info = 1;
			break;
		case 'h':
			usage();
			break;
		default:
			usage();
		}
	}

	if (!device)
		usage();

	fd = open(device, O_RDWR);
	if (fd < 0) {
		printf("Error in opening the device %d\n", fd);
		return -1;
	}

	if (ioctl(fd, I2C_SLAVE_FORCE, I2C_DEV_ADDR) < 0) {
		printf("Error in communicating with the device \n");
		return -1;
	}

	for( int i = 0; i < 15; i++) {
		if (!switch_hid_to_i2c()) {
			success = 1;
			break;
		}
	}
	if(!success) {
		printf("Failed to switch to I2C mode\n");
		goto out;
	}

	configuration config;
	read_config(&config);

	if (version) {
		if (version_check_test(config) < 0)
			printf("Firmware Version Test: FAILED\n\n");
		else
			printf("Firmware Version Test: SUCCESS\n\n");
	}

	if (rawdata) {
		if (rawdata_test(config) < 0)
			printf("Rawdata Test: FAILED\n\n");
		else
			printf("Rawdata Test: SUCCESS\n\n");
	}

	if (panel_differ) {
		if (panel_differ_test(config) < 0)
			printf("Panel Differ Test: FAILED\n\n");
		else
			printf("Panel Differ Test: SUCCESS\n\n");
	}

	if (scap_rawdata) {
		if (scap_rawdata_test(config) < 0)
			printf("Scap Rawdata Test: FAILED\n\n");
		else
			printf("Scap Rawdata Test: SUCCESS\n\n");
	}

	if (scap_cb) {
		if (scap_cb_test(config) < 0)
			printf("Scap CB Test: FAILED\n\n");
		else
			printf("Scap CB Test: SUCCESS\n\n");
	}

	if(ic_info) {
		if(ic_display_info(config) < 0)
			printf("Failed to display IC info\n\n");
	}

	ret = 0;
out:
	if (switch_i2c_to_hid() < 0) {
		printf("Failed to switch to HID mode\n");
		ret = -1;
	}

	return ret;
}
