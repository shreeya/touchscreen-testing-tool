#define ONE_THROUGH_16(some_macro, args...) \
	some_macro(args, 1) \
	some_macro(args, 2) \
	some_macro(args, 3) \
	some_macro(args, 4) \
	some_macro(args, 5) \
	some_macro(args, 6) \
	some_macro(args, 7) \
	some_macro(args, 8) \
	some_macro(args, 9) \
	some_macro(args, 10) \
	some_macro(args, 11) \
	some_macro(args, 12) \
	some_macro(args, 13) \
	some_macro(args, 14) \
	some_macro(args, 15) \
	some_macro(args, 16)

#define struct_key_val_n(key, n) const char* key##n;
#define define_read_key_val_n(key, n) int defined_##key##n;

struct array_of_values {
	int* array;
	int length;
};

struct array_of_values parse_array_of_values(const char* array_string);

void free_array_of_values(struct array_of_values x);

typedef struct
{
	int SCapCbTest_OFF_Min;
	int SCapCbTest_OFF_Max;
	int SCapCbTest_ON_Min;
	int SCapCbTest_ON_Max;
	int SCapRawDataTest_OFF_Min;
	int SCapRawDataTest_OFF_Max;
	int SCapRawDataTest_ON_Min;
	int SCapRawDataTest_ON_Max;
	int FW_VER_VALUE;
	int PanelDifferTest_Min;
	int PanelDifferTest_Max;
	int RawDataTest_Low_Min;
	int RawDataTest_Low_Max;
	int RawDataTest_High_Min;
	int RawDataTest_High_Max;

	ONE_THROUGH_16(struct_key_val_n, RawData_Max_High_Tx)
	ONE_THROUGH_16(struct_key_val_n, RawData_Min_High_Tx)
	ONE_THROUGH_16(struct_key_val_n, Panel_Differ_Max_Tx)
	ONE_THROUGH_16(struct_key_val_n, Panel_Differ_Min_Tx)

	ONE_THROUGH_16(define_read_key_val_n, RawData_Max_High_Tx)
	ONE_THROUGH_16(define_read_key_val_n, RawData_Min_High_Tx)
	ONE_THROUGH_16(define_read_key_val_n, Panel_Differ_Max_Tx)
	ONE_THROUGH_16(define_read_key_val_n, Panel_Differ_Min_Tx)

	int is_defined_RawData_Max_High_Tx;
	int is_defined_RawData_Min_High_Tx;
	int is_defined_Panel_Differ_Max_Tx;
	int is_defined_Panel_Differ_Min_Tx;

	struct array_of_values RawData_Min_High_Tx;
	struct array_of_values RawData_Max_High_Tx;
	struct array_of_values Panel_Differ_Max_Tx;
	struct array_of_values Panel_Differ_Min_Tx;
} configuration;

int read_config(configuration* config);
void set_is_defined(configuration* config);
